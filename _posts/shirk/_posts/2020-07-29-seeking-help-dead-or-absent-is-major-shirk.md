﻿---
layout: post
title: "Seeking the help of the dead or the absent is major Shirk"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Da'imah no. 9272-2"
category: ["tawassul", awliya, shirk]
hijri: Dhu al-Hijjah 8, 1441 AH
date: 2020/07/29
shaykhs: 
 - Shaykh Abdullah ibn Ghudayyan
 - Shaykh Abdul-Aziz ibn Baz
 - Shaykh Abdul-Razzaq al-Afify
---

Question: 

Is seeking help from someone who is absent or dead considered an act of major Kufr (disbelief)?

Answer:

Yes, seeking help from someone who is dead or absent is considered an act of major Shirk (associating others with Allah in His Divinity or worship) and doing so puts the doer outside the fold of Islam, for Allah (Glorified be He) says, "**And whoever invokes (or worships), besides Allâh, any other ilâh (god), of whom he has no proof; then his reckoning is only with his Lord. Surely! Al-Kâfirûn (the disbelievers in Allâh and in the Oneness of Allâh, polytheists, pagans, idolaters) will not be successful.**" [al-Mu'minun: 117]

Allah (Glorified and Exalted be He) also says, "**Such is Allâh, your Lord; His is the kingdom. And those, whom you invoke or call upon instead of Him, own not even a Qitmîr (the thin membrane over the date-stone). If you invoke (or call upon) them, they hear not your call; and if (in case) they were to hear, they could not grant it (your request) to you. And on the Day of Resurrection, they will disown your worshipping them. And none can inform you (O Muhammad صلى الله عليه وسلم) like Him Who is the All-Knower (of everything).**" [Fatir: 13-14]

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.
