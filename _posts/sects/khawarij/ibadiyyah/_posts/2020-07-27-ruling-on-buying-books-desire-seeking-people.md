---
layout: post
title: "Ruling on buying books from desire-seeking people"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Daimah no. 21394-5"
category: [knowledge, books, ibadiyyah, khawarij]
hijri: Dhu al-Hijjah 6, 1441 AH
date: 2020/07/27
shaykhs: 
 - Shaykh Salih Fawzan
 - Shaykh Abdul-Aziz Aal al-Shaykh
 - Shaykh Bakr Abu Zayd
 - Shaykh Abdullah ibn Ghudayyan
---

Question: 
 
Is it permissible to do business with ibadiyyah (a Muslim sect)? For example, I buy their good books and cassettes, should I stop or continue doing this?

Answer:

There is no harm in buying the useful books from anyone because of their great benefit. As for the books that contain heresy and error, it is not permissible to deal in them.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.


