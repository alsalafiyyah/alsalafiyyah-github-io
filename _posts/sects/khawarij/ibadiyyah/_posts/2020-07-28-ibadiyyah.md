---
layout: post
title: "Al-Ibadiyyah"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Da'imah no. 6935-5"
category: [khawarij, ibadiyyah]
hijri: Dhu al-Hijjah 7, 1441 AH
date: 2020/07/28
shaykhs: 
 - Shaykh Abdul-Aziz ibn Baz
 - Shaykh Abdul-Razzaq al-Afify
 - Shaykh Abdullah ibn Ghudayyan
 - Shaykh Abdullah ibn Qa'ud
---

Question: 

Is Al-Ibadiyyah regarded as one of the deviant sects of Al-Khawarij, and is it permissible to pray behind them?

Answer:

Al-Ibadiyyah is one of the deviant sects because of their aggression and rebellion against 'Uthman ibn 'Affan and 'Ali (may Allah be pleased with them both), and it is not permissible to pray behind them.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.
