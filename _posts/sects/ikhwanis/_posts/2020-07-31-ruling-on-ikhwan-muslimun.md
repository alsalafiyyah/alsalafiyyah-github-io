---
layout: post
title: "The ruling on Al-Ikhwan-ul-Muslimun (the Muslim Brotherhood) group"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Daimah no. 9018-1"
category: [ikhwanis]
hijri: Dhu al-Hijjah 11, 1441 AH
date: 2020/07/31
shaykhs: 
 - Shaykh Abdul-Aziz ibn Baz
 - Shaykh Abdul-Razzaq al-Afify
 - Shaykh Abdullah ibn Ghudayyan
 - Shaykh Abdullah ibn Qa'ud
---

Question: 

What is the ruling on Al-Ikhwan-ul-Muslimun (the Muslim Brotherhood) group? Are they adherents of Bid'ah (innovation in religion) or are they too lenient? What is the method which should be followed after the Qur'an and the Sunnah?

Answer:

The method which should be followed is the Qur'an and the Sunnah. A person should seek the help of Allah (Exalted be He), then the views of the Salaf (righteous predecessors) in order to understand them. Whoever follows Al-Ikhwan or other groups and adheres to the Qur'an and the Sunnah in word and deed is from Ahl-ul-Sunnah wal-Jama`ah (adherents to the Sunnah and the Muslim mainstream). At the same time, whoever neglects or adds anything to them is lenient or extravagant to the extent that they disobey and innovate in religion.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.
