---
layout: post
title: "Ruling on eating animals slaughtered by Ja'faris Shi'ah"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Da'imah no. 1661"
category: [shiah, shiites, rafidah]
hijri: Dhu al-Hijjah 7, 1441 AH
date: 2020/07/28
shaykhs: 
 - Shaykh Abdul-Aziz ibn Baz
 - Shaykh Abdul-Razzaq al-Afify
 - Shaykh Abdullah ibn Ghudayyan
---

Question: 

The person asking this question along with a group of people live at the northern borders close to the Iraqi centers. There is a Shiite community belonging to the Ja'fari school of thought. Some people refuse to eat the meat of animals sacrificed by these Ja'fari people. Others eat from it. We wonder if it is lawful to eat this meat or not taking into consideration that they invoke 'Ali, Al-Hasan, Al-Husayn, and their leaders in times of adversity or prosperity.

Answer:

If the situation is as the questioner mentioned that the Shiites of the Ja'fari sect invoke 'Ali, Al-Hasan, Al-Husayn, and their leaders, they are committing an act of Shirk (associating others with Allah in His Divinity or worship) and thus they are apostates. Therefore, it is unlawful to eat from the meat of the animals they sacrifice, for it is considered meat of dead animals even if they mention Allah's Name when slaughtering them.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.


