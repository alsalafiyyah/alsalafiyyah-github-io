---
layout: post
title: "Ruling on Imamite Shi'ah sect in Islam and its origin"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Da'imah no. 9420-3"
category: [shiah, shiites, rafidah]
hijri: Dhu al-Hijjah 7, 1441 AH
date: 2020/07/28
shaykhs: 
 - Shaykh Abdul-Aziz ibn Baz
 - Shaykh Abdul-Razzaq al-Afify
 - Shaykh Abdullah ibn Ghudayyan
---

Question: 

Is Imamite Shi'ah an Islamic sect? Who is the founder? In fact, the Shi'ah claim their sect is traceable back to 'Ali (may Allah honor his face). However, if the sect of the Shiites is not Islamic, in what way will it be non-Islamic? I hope Your Eminence graciously gives me a detailed account provided with valid evidence of the Shiite sect and their beliefs along with highlighting some innovative Islamic sects.

Answer:

The fundamentals and secondary elements of the Imamite Shi'ah are Bid'ahs (innovations in religion). You are advised to consult the books "Al-Khututul-'Aridah", "Al-Tuhfa Al-Ithna 'Ashariyyah Abridged" and "Minhaj-us-Sunnah" by Shaykh-ul-Islam ibn Taymiyah for an account of many of their Bid'ahs.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.


