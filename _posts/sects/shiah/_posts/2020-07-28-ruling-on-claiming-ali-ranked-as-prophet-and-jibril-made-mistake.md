---
layout: post
title: "Ruling on claiming that 'Ali is ranked as a prophet and Jibril made a mistake"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Da'imah no. 8564"
category: [shiah, shiites, rafidah]
hijri: Dhu al-Hijjah 7, 1441 AH
date: 2020/07/28
shaykhs: 
 - Shaykh Abdul-Aziz ibn Baz
 - Shaykh Abdul-Razzaq al-Afify
 - Shaykh Abdullah ibn Ghudayyan
 - Shaykh Abdullah ibn Qa'ud
---

Question: 

What is the ruling on Shiites especially those who say that 'Ali is ranked as a prophet and that Jibril (Gabriel) (peace be upon him) mistakenly descended to the Prophet Muhammad?

Answer:

Shiites have been divided into many sects. Whoever among them says that 'Ali (may Allah be pleased with him) is ranked as a prophet and that Jibril (Gabriel) (peace be upon him) mistakenly descended to the Prophet Muhammad (peace be upon him) counts as Kafir (disbeliever).

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.


