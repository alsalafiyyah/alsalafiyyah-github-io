---
layout: post
title: "Scholars' books on Shiite 'Aqidah and their details"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Da'imah no. 7308-1"
category: [shiah, shiites, rafidah, books]
hijri: Dhu al-Hijjah 7, 1441 AH
date: 2020/07/28
shaykhs: 
 - Shaykh Abdul-Aziz ibn Baz
 - Shaykh Abdul-Razzaq al-Afify
 - Shaykh Abdullah ibn Ghudayyan
 - Shaykh Abdullah ibn Qa'ud
---

Question: 

What is the Shiite 'Aqidah (creed)? Please clarify as much as possible.

Answer:

Shiites have numerous sects, some are extremists and others are not. Hence, we recommend you read books written by scholars about their sects and the 'Aqidah (creed) of each. These books include Maqalat Al-Islamiyyin, by Abu Al-Hasan Al-Ash'ary; Minhaj Al-Sunnah, by Shaykh Al-Islam Ibn Taymiyyah; Al-Farq bayn Al-Firaq, by 'Abdul-Qadir Al-Baghdady; Al-Milal wal Nihal, by Al-Shahristany; Al-Fisal fi Al-Milal wa Al-Nihal, by Ibn Hazm; and Mukhtasar Kitab Al-A'immah Al-Ithna 'Ashriyyah, and the like. Here you will get adequate information about their creeds.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.


