---
layout: post
title: "The difference between Sunnis and Shi'ah"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Da'imah no. 8852-1"
category: [shiah, shiites, rafidah]
hijri: Dhu al-Hijjah 7, 1441 AH
date: 2020/07/28
shaykhs: 
 - Shaykh Abdul-Aziz ibn Baz
 - Shaykh Abdul-Razzaq al-Afify
 - Shaykh Abdullah ibn Ghudayyan
 - Shaykh Abdullah ibn Qa'ud
---

Question: 

Please clarify the differences between Sunnis and Shi`ah (Shi'ites) Which of them is the closest to the Sunnah?

Answer:

The differences between Ahl-us-Sunnah wal-Jama'ah (adherents to the Sunnah and the Muslim mainstream) and the Shiites are major regarding Tawhid (Oneness of Allah), Prophecy, Imamah (leadership or governance) and more. There are many books in this regard, including Minhaj Al-Sunnah by the Shaykh of Islam Ibn Taymiyyah, Al-Milal wal-Nihal by Al-Shahristany, Al-Fisal by Ibn Hazm, Al-Khutut Al-'Aridah by Muhibb Al-Din Al-Khatib, and Mukhtasar Al-Tuhfa Al-Ithna Ashariyyah. You can read about this issue in the above mentioned books.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.


