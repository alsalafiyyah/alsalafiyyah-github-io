---
layout: post
title: "Ba'ath Party"
publisher: "alsalafiyyah@icloud.com"
source: "Majmu' Fatawa wa Maqalat 28/270"
category: [baath, refutals]
hijri: Dhu al-Hijjah 3, 1441 AH
date: 2020/07/24
shaykhs: Shaykh Ibn Baz
---

Question: 

Are president of Iraq and members of the Ba'ath Party that he led considered Kafirs (disbelievers) or not? 

Answer:

All Ba'athists are Kafirs including the president of Iraq as they oppose Shari`ah (Islamic law) and feud with it.


