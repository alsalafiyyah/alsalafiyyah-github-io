---
layout: post
title: "Ruling on joining Jama'at Tabligh"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Daimah no. 17776-2"
category: [sufism, tablighis]
hijri: Dhu al-Hijjah 5, 1441 AH
date: 2020/07/26
shaykhs: 
 - Shaykh Ibn Baz
 - Shaykh Abdul-Aziz bin Abdullah Aal Al-Shaykh
 - Shaykh Salih Fawzan
 - Shaykh Abdullah ibn Ghudayyan
 - Shaykh Bakr Abu Zayd
---

Question: 

I joined this group and made use of the knowledge they provide. However, they observe some acts that are not mentioned in the Book of Allah or in the Sunnah. The following are some examples:

1. Forming circles of two people or more inside the Masjid (mosque) to study the last ten Surahs of the Qur'an and whenever they go out on a mission they have to perform this task mentioned above.
2. Continuous performance of I`tikaf on Thursdays. 
3. Determining certain days for setting out for Da`wah, three days of every month, forty days of every year, and four months during one's lifetime.
4. Continuous collective supplication after each time of making Da`wah.

If I join this group, what should I do with these acts which are not mentioned in the Book of Allah or in the Sunnah? Furthermore, changing the group's method is impossible. Please, advise.

Answer:

The acts of the group you mentioned count as Bid`ahs (innovations in religion). You are not allowed to join them unless they adhere to the teachings stated in the Book of Allah and in the Sunnah and stop observing Bid'ah in deeds, words and beliefs. 

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.
