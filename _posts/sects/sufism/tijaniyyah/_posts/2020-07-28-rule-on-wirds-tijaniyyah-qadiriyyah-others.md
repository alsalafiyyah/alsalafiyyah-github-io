---
layout: post
title: "Ruling on the Wirds of Al-Tijaniyyah, Al-Qadiriyyah and others"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Da'imah no. 6460-6"
category: ["sufism", tijaniyyah, qadiriyyah]
hijri: Dhu al-Hijjah 7, 1441 AH
date: 2020/07/28
shaykhs: 
 - Shaykh Abdul-Aziz ibn Baz
 - Shaykh Abdullah ibn Ghudayyan
 - Shaykh Abdullah ibn Qa'ud
 - Shaykh Abdul-Razzaq al-Afify
---

Question: 

What is the ruling on the Wird (sayings recited with consistency) of the Tijaniyyah, Qadiriyyah and other Sufi Tariqahs (orders)?

Answer:

Their Wirds are full of the Bid'ah (rejected innovations in religion) and innovated Dhikr (Remembrances of Allah) like other Sufi orders. It is better for Muslims to take Wird for themselves from the Qur'an and remember Allah with Dhikr reported from the Prophet (peace be upon him).

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.
