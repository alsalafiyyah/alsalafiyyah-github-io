---
layout: post
title: "Ruling on the recitation of Al-Tijaniyyah and Al-Qadiriyyah and praying behind their followers"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Da'imah no. 4150-3"
category: ["sufism", tijaniyyah, qadiriyyah]
hijri: Dhu al-Hijjah 7, 1441 AH
date: 2020/07/28
shaykhs: 
 - Shaykh Abdul-Aziz ibn Baz
 - Shaykh Abdullah ibn Ghudayyan
 - Shaykh Abdullah ibn Qa'ud
 - Shaykh Abdul-Razzaq al-Afify
---

Question: 

What is the ruling on reciting the utterances said on a regular basis by Al-Tijaniyyah and Al-Qadiriyyah Tariqahs (Sufi orders)? And what is the ruling on whoever recites them till death? Is it permissible to perform Salah (Prayer) behind such a person and is it permissible to offer the funeral prayer for them after their death? Appreciate your guidance, may Allah guide you.

Answer:

The utterances said on a regular basis by Al-Tijaniyyah and Al-Qadiriyyah is full of superstitions and Bid'ahs (rejected innovation in religion) that lead to Shirk (associating others with Allah in His Divinity or worship), such as seeking help from people other than Allah and the kinds of Dhikr that are not derived from the Qur'an or the Sunnah of the Prophet (peace be upon him). Therefore, it is not permissible to supplicate to Allah through these utterances, offer Salah behind people who worship Allah through them, or perform the funeral prayer for them after their death. As for their end, this is up to Allah who knows the secrets and that which is yet more hidden.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.
