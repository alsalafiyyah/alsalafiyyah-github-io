---
layout: post
title: "Sufi superstitions"
publisher: "alsalafiyyah@icloud.com"
category: sufism
source: "Fatwas of the Permanent Committee of KSA, question 1"
hijri: Dhul-Qa'dah 20, 1441 AH
shaykhs: 
 - Shaykh Ibn Baz
 - Shaykh Abdul-Aziz Aal Al-Shaykh
 - Shaykh Salih Fawzan
 - Shaykh Abdullah ibn Ghudayyan
 - Shaykh Bakr abu Zayd
---

Question: 

The mentioned Imam says that one day Safi Al-Din ibn Ahmad passed along a road in that city and found a rock. He divided the rock with his Miswak (tooth-cleansing stick) into four parts and ordered one part of it to go to Iraq and ordered other parts to go to other different places of the same city. He left part of the rock on the mount in the city of Sabr according to him. Is this a Karamah (an extraordinary event caused by Allah for or through a pious person) of pious people as some people claim, or is it done by help from the Jinn? 

Answer: 

What is mentioned in the question is a false and superstitious talk and it is not permissible to believe it; because this is a means leading to Shirk (associating others with Allah in His Divinity or worship). 

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions. 
