---
layout: post
title: "What is the ruling of Islam on the Sufi orders that exist today"
publisher: "alsalafiyyah@icloud.com"
category: sufism
source: "Fatwas of the Permanent Committee of KSA no.6433-5"
hijri: Dhul-Qa'dah 11, 1441 AH
date: 2020-07-02
shaykhs: 
 - Shaykh Ibn Baz
 - Shaykh Abdul-Razzaq al-Afify
 - Shaykh Abdullah ibn Ghudayyan
 - Shaykh Abdullah ibn Qa'ud
---

Question: 

What is the ruling of Islam on the Sufi orders that exist today?

Answer: 

Working according to Bid'ahs (innovations in religion) prevails in most of the Sufi orders. We advise you to follow the guidance of the Prophet (peace be upon him) and his Companions with regard to acts of worship and other things. You can read, the book written by 'Abdul Rahman Al-Wakil (may Allah be Merciful with him) entitled, "Hadhhi Hiya Al-Sufiyyah (This is Sufism)" in this regard.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions. 
