---
layout: post
title: "What is Sufism"
publisher: "alsalafiyyah@icloud.com"
category: sufism
source: "Fatwas of the Permanent Committee of KSA no. 17558-2"
hijri: Dhul-Qa'dah 20, 1441 AH
shaykhs: 
 - Shaykh Ibn Baz
 - Shaykh Abdul-Aziz Aal Al-Shaykh
 - Shaykh Salih Fawzan
 - Shaykh Bakr abu Zayd
---

Question: 

What is Sufism? Does it pose a danger to one's Islam? What is the ruling on keeping company with Sufis? 

Answer: 

In general, Sufism as it exists at the present time refers to a misguided group, which adopts a Manhaj (methodology) in 'Ibadah (worship) that opposes the Sunnah. The followers of Sufism receive their teachings from their leaders and shaykhs of Tariqah (Sufi order) whom they believe can bring benefit and cause harm besides Allah. Therefore, it is not permissible to keep company with or befriend them except to call them to Allah and help them to correctly understand the Sunnah.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions. 
