---
layout: post
title: "The pledge taken from a Sufi Shaykh"
publisher: "alsalafiyyah@icloud.com"
category: sufism
source: "Fatwas of the Permanent Committee of KSA no.16098-2"
hijri: Dhul-Qa'dah 20, 1441 AH
shaykhs: 
 - Shaykh Abdul-Aziz bin Baz
 - Shaykh Abdul-Razzaq al-Afify
 - Shaykh Abdul-Aziz Aal ash-Shaykh
 - Shaykh Salih Fawzan
 - Shaykh Abdullah bin Ghudayyan
 - Shaykh Bakr abu Zayd
---

Question: 

What is the ruling of Islam on the pledge that we make to the shaykh of a Tariqah (Sufi order)? Will we be considered disobeying the Qur'an and the Sunnah (whatever is reported from the Prophet) if we break it? 

Answer: 

It is not permissible to make a pledge of allegiance to anyone except to a Muslim ruler. It is not permissible to make it to the shaykh of a Sufi order or any other person, for this has not been authentically reported from the Prophet (peace be upon him). Every Muslim should worship Allah according to the way He prescribed without committing oneself to a certain person, for this is the attitude of the Christians toward their priests and bishops, a thing that does not exist in Islam.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions. 

