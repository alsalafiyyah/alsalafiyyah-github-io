---
layout: audio
title: "All the Sufi orders are innovations in the religion"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Nur 'Ala al-Darb vol 3"
category: ["audios", "sufism", khalwatiyyah, qadiriyyah, shadhiliyyah]
mp3: https://files.zadapps.info/binbaz.org.sa/fatawa/nour_3la_aldarb/nour_463/46305.mp3
link: https://cutt.ly/tdGpd4W
hijri: Dhul-Hijjah 17, 1441 AH
date: 2020/08/7
shaykhs: Shaykh Abdul-Aziz ibn Baz
---

Question:

I am a follower of a Khalwati Shaykh and I am quite sure that he is a good man and a 
very righteous person. It is to be noted that anyone following his order becomes 
righteous and repents to Allah. Some of those who repented used to commit evil actions. This question is presented to you, because you mentioned that all the orders of Sufism are innovations in the religion. Please tell me about my situation, may Allah guide you. Should I continue following this way or should I abandon it? May Allah reward you with the best. 

Answer:

Yes, all the Sufi orders are innovations in the religion. However, some actions of these ways might match with the truth. Therefore, what is in agreement with the truth should be adopted, because of being in consonance with the truth and not because of following the Khalwati, Al-Qadiri, or Al-Shadhili order or any other order. What is good in their order should be accepted, because it is in agreement with the Shar'ah and what is in discordance with the Sharfah should be deserted. If you want us to explain to you what is good and what is bad of these ways, please tell us about the way you are following. Explain it to us in another question and we will, Allah willing, explain to you the good and the bad thereof. 
