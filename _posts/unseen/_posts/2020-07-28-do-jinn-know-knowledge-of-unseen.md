---
layout: post
title: "Do the Jinn know knowledge of Unseen?"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Da'imah no. 18645-3"
category: [jinns, unseen, ghayb]
hijri: Dhu al-Hijjah 7, 1441 AH
date: 2020/07/28
shaykhs: 
 - Shaykh Abdul-Aziz ibn Abdullah Aal Al-Shaykh
 - Shaykh Abdul-Aziz ibn Baz
 - Shaykh Salih Al-Fawzan
 - Shaykh Bakr Abu Zayd
---

Question: 

Do the Jinn (creatures created from fire) know the Ghayb (the Unseen)? Please clarify this as soon as possible.

Answer:

Knowledge of Ghayb is one of the exclusive attributes of Rububiyyah (Lordship); no one knows the Ghayb of the heavens and the earth except Allah. Allah (Exalted be He) says: "And with Him are the keys of the Ghaib (all that is hidden), none knows them but He." [al-An'am: 59] 

He (Exalted is His Majesty) also says: "Say: “None in the heavens and the earth knows the Ghaib (Unseen) except Allâh” [an-Naml: 65] 

The Jinn do not know the Ghayb, which is proven by His (Exalted be He) Saying: "Then when We decreed death for him [Sulaimân (Solomon)], nothing informed them (jinn) of his death except a little worm of the earth, which kept (slowly) gnawing away at his stick. So when he fell down, the jinn saw clearly that if they had known the Unseen, they would not have stayed in the humiliating torment." [Saba: 14] 

Therefore, anyone who claims to know the Ghayb is a Kafir (disbeliever), and anyone who believes in this claim is a Kafir as well, for they are denying the Qur'an.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.


