---
layout: post
title: "Ruling on sitting with Mubtadi's (ones who introduces innovations in religion)"
publisher: "alsalafiyyah@icloud.com"
source: "Majmu' Fatawa wa Maqalat 28/267"
category: [misc, creeds, innovators, ibnbaz]
hijri: Dhu al-Hijjah 5, 1441 AH
date: 2020/07/26
shaykhs: Shaykh Ibn Baz
---

Question: 

Is it permissible to sit with people who indulge in Bid`ahs (innovations in religion) and participate in their lessons? 

Answer:

It is not permissible for you to sit with or consider them friends. You have to forbid them from doing such Bid`ahs and warn them against their dangers. May Allah protect us!
