---
layout: audio
title: "They say that Imam Muhammad Ibn Abdul-Wahhab removed all the domes, with the exception of that green dome in Prophet's Mosque"
publisher: "alsalafiyyah@icloud.com"
hijri: Dhul-Hijjah 24, 1441 AH
date: 2020/08/14
category: [wahhabism]
uid: green-dome
mp3: https://files.zadapps.info/binbaz.org.sa/fatawa/nour_3la_aldarb/nour_864/nour_86402.mp3
shaykhs: Shaykh Abdul-Aziz ibn Baz
---


Question: 

I know that building domes on graves is impermissible; however, some people say it is permissible and they take the dome on the grave of the Prophet (peace be upon him) as evidence to support their stance. They say that Muhammad Ibn Abdul-Wahhab removed all the domes, with the exception of that dome; namely, the dome of the Prophet (peace be upon him). How can we refute the argument of such people? Appreciate your guidance, may Allah guide you. 

Answer: 

There is no doubt that building domes on graves is Bid'ah and Munkar like building Masjids thereon, for it was proven that the Messenger of Allah (peace be upon him) said: May Allah curse the Jews and the Christians for taking the graves of their prophets as places of worship Moreover, it was proven that the Prophet (peace be upon him) said: "Those before you used to take the graves of their prophets and pious people as places of worship. Do not take them as Masjids (places of worship). I forbid you from that." (Related by Muslim in his Sahih (authentic) Book of Hadith). 

Furthermore, it was authentically related on the authority of Jabir ibn Abdullah (may Allah be pleased with him) in Sahih Muslim that the Prophet (peace be upon him) forbade plastering the graves, sitting on them and building on them. Undoubtedly, putting domes on graves is one kind of building and so is building Masjids on them, ceilings and walls. 

Rather, graves should remain apparent on the ground just as they used to be during the lifetime of the Prophet (peace be upon him) in Al-Baqi' and in other places. The grave should be almost one hand span higher than the ground so that people would know it is a grave and thus, would not misuse it. However, it is irnper rnissible to build a dome on it, a room or an arbor. Graves should remain apparent as they are and nothing should be added to them except their own dust from which they were dug. It should be raised one hand span as mentioned in the Hadith of Sad ibn Abu Waqqas. He (may Allah be pleased with him) said: "Bury me in a Lahd (a crevice on the side of a grave facing the direction faced for Prayer) and put unburnt bricks on me as was done with the Messenger of Allah (peace be upon him)." 

In another narration he said: "His grave was raised a hand span from the ground (i.e. the grave of the Prophet (peace be upon him)." 

So, graves should be raised a hand span from the ground to make people aware that they are graves and in this way, they would not be misused. Thus, people would refrain from treading on them or sitting on them. However, nothing should be built on them; neither a dome nor anything else as indicated by the previous Hadiths; the Hadith of Jabir and the Hadith of 'Aishah and others. In the Hadith of Jabir, the Prophet (peace be upon him) prohibited building on or plastering graves. 

The dome that is on the Prophet's (peace be upon him) grave was introduced by some of the Turkish rulers in the later centuries; perhaps the ninth or eighth century. People did not remove it for many reasons. Among these reasons was the ignorance of many of those who ruled Madinah. Another reason was the fear of Fitnah. They feared that people might rise against them and accuse them of hating the Prophet (peace be upon him) and of other false charges. This is why the Saudi government is keeping this dome. If it is removed, some ignorant people, and most of the people are ignorant, might say that those people removed the dome because they hate the Prophet (peace be upon him). They would not say it was removed because it is a Bid'ah; rather, they would accuse them of hating the Prophet (peace be upon him). 

Such are the words of the ignorant and those like them. Thus, the Saudi government has left that dome for fear of Fitnah and for fear that people might think ill of them. However, there is no doubt that this government believes in the prohibition of building on graves and constructing domes thereon. The Messenger of Allah (peace be upon him) was buried in the house of Aishah so that there would be no Fitnah and to be sure that people would not exceed the proper limits regarding the Prophet (peace be upon him). The Sahabah (Companions of the Prophet) buried him in the house of Aishah to avoid Fitnah and for fear that the ignorant might be tempted by it. The dome on the Prophet's (peace be upon him) grave was put there later by some ignorant rulers. 

The right thing, and it is permissible, would be to remove it, however, some ignorant people might not accept that and might think that those who remove it are wrong and that they hate the Prophet (peace be upon him). Hence, it was left by the Saudi government as it is because it was built by others and the Saudi government is not in favor of arousing any confusion or Fitnah that might be led by those who worship graves and exceed the proper limits regarding the dead from among the Mushriks. Thus, they would be accused of false charges including hatred of the Prophet (peace be upon him) or failing to show proper reverence for him. 

However, the Saudi scholars, and among them Shaykh Muhammad Ibn Abdul-Wahhab (may Allah be merciful with him), are all adherent to the Sunnah and follow the way of the Companions of the Prophet (peace be upon him) and their followers in terms of adopting Tawhid (belief in the Oneness of Allah/ monotheism), worshiping Allah with foil sincerity, and warning of Shirk, Bid'ah or the means of Shirk. They revere the Prophet (peace be upon him) and his Companions (may Allah be pleased with them) more than anyone else. They, more than others, follow the example of the Salaf (righteous predecessors) in their love for the Prophet (peace be upon him) and revering him. It is that Shar'i reverence that implies no excess and no Bid'ah. Rather, it is reverence that implies following Shar'ah, glorifying his commands and prohibitions, defending his Sunnah, calling people to follow him and warning them against Shirk, Munkar and Bid'ah. They all follow that path; calling people to follow the Messenger of Allah (peace be upon him) and to glorify his Sunnah, to offer acts of worship with foil sincerity to Allah and warning them about committing Shirk and the Bid'ah that have become widespread among people for ages now. 

This dome that was put on the grave of the Prophet (peace be upon him) is a Bid'ah and it was left only to avoid ill thoughts and Fitnah. May Allah grant us success. 



