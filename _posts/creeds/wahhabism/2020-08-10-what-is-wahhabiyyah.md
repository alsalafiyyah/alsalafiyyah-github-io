﻿---
layout: post
title: "What is Wahhabiyyah"
publisher: "alsalafiyyah@icloud.com"
language: en
source: "Fatawa Al-Lajnah Ad-Da'imah no. 9420-2"
hijri: Dhul-Hijjah 19, 1441 AH
date: 2020/08/10
category: [wahhabism]
uid: what-is-wahhabism
shaykhs: 
 - Shaykh Abdul-Aziz ibn Baz
 - Shaykh Abdul-Razzaq al-Afify
 - Shaykh Abdullah ibn Ghudayyan
---

Question: 

What is Wahhabism?

Answer: 

Wahhabism is the term which the enemies of Shaykh Muhammad ibn 'Abdul-Wahhab (may Allah be merciful to him) gave to his call to purify Tawhid (monotheism) from all forms of Shirk (associating others with Allah in His Divinity or worship) and to renounce all the ways except that of Muhammad Ibn ‘Abdullah (peace be upon him). Their intention was to alienate people from his call and obstruct it. However, the call was not affected in the least. On the contrary, it spread further in the world, and attracted more the interest of people whom Allah has guided to seek to learn more about the nature of this call and its purposes, and the evidences from the Qur’an and the authentic Sunnah it relies on. So, people became more attached to it and began calling other people to it, all praise and thanks to Allah.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.

