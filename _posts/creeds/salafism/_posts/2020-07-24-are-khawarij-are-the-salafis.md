---
layout: post
title: "The Reality on the 'Aqidah (creed) of Al-Khawarij"
publisher: "alsalafiyyah@icloud.com"
source: "Majmu' Fatawa wa Maqalat 28/254"
category: [salafism, khawarij]
hijri: Dhu al-Hijjah 3, 1441 AH
date: 2020/07/24
shaykhs: Shaykh Ibn Baz
---

Question: 

What do you say to one who believes that the 'Aqidah (creed) of Al-Khawarij followed the 'Aqidah (creed) of Salafiyyah (following the way of the righteous predecessors) and that they, i.e. Al-Khawarij are Salafis (those who following the way of the righteous predecessors)?

Answer: 

This is a false claim that was nullified by the Prophet (peace be upon him) who stated regarding Al-Khawarij: A group of rebels will emerge from my Ummah when they are separated. If you compare your prayers with their prayers and your recitation with their recitation, you will look down upon your prayer and recitation, in comparison to theirs. Yet they will go out of Islam as an arrow goes through the body of the prey. Kill them wherever you find them, for whoever kills them shall have reward." [^1] There is another narration reported from the Prophet (peace be upon him) regarding Al-Khawarij: "They would kill Muslims and spare the idolaters." [^2]

It is known from their 'Aqidah (creed) that they consider disobedient Muslims to be Kafirs (disbelievers) and judge that they will remain in Hellfire forever. This is why they fought against 'Ali (may Allah be pleased with him) and those who allied with him from among the Prophet's Companions and others. 'Ali fought against and killed them on the day of Al-Nahrawan. May Allah be pleased with 'Ali and all the Companions! May Allah grant us success.

[^1]: Related by Al-Bukhari [Book on merits and virtues, Chapter on the signs of prophethood in Islam] no. 3611.
[^2]: Related by Al-Bukhari [Book on prophets, Chapter on the words of Allah, the Almighty: "The Ad, they were destroyed by a furious Wind"] no. 3344.

