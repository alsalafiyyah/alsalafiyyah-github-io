---
layout: post
title: "Is it correct that Hanbalis are the only Salafis"
publisher: "alsalafiyyah@icloud.com"
category: [hanbalis, salafism]
source: "Majmu' fatawa of Shaykh ibn Baz 9/238"
hijri: Dhul-Qa'dah 26, 1441 AH
source: Shaykh ibn Baz
---

Question:

Is it correct that Al-Hanabilah (Hanbalis, the followers of Imam Ahmad's School of Jurisprudence) are the only Salafiyyah (those following the way of the righteous predecessors)? What is the truth about the Salafiyyah? Is it a synonym for strictness and rigidity as some propagate? 

Answer:

This is not true. The Salaf (righteous predecessors) were the Sahabah (Companions of the Prophet, may Allah be pleased with them) and those who followed their way from among the Tabi'un (Followers, the generation after the Companions of the Prophet) and their successors among Al-Hanafiyyah (the followers of Imam Abu Hanifah's School of Jurisprudence), Al-Malikiyyah, Al-Shafi'iyyah Al-Hanabilah and others who followed Al-Haqq (the Truth) and adhered to the glorious Qur'an and the purified Sunnah (whatever is reported from the Prophet) in regard to Tawhid (belief in the Oneness of Allah/ monotheism), the Names and Attributes of Allah, and all matters of the Din (religion of Islam). We ask Allah to make us among them and to help all Muslims everywhere - governments and people - to adhere to His Glorious Qur'an and the Sunnah of His Honest Messenger, to rule by them, to seek judgments from them, and to avoid anything that contradicts them. He is the Guardian of this and the One Who is Capable of doing so. May Allah grant us success. 
 
