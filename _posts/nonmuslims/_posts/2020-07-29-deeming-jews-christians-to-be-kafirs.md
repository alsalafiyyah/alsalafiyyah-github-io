---
layout: post
title: "Deeming Jews and Christians, who do not believe in Muhammad, to be disbelievers"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Da'imah no. 6505-14"
category: [nonmuslims, disbelievers, walabara]
hijri: Dhu al-Hijjah 8, 1441 AH
date: 2020/07/29
shaykhs: 
 - Shaykh Abdul-Aziz ibn Baz
 - Shaykh Abdul-Razzaq al-Afify
 - Shaykh Abdullah ibn Ghudayyan
 - Shaykh Abdullah ibn Qa'ud
---

Question: 

What is the ruling of Islam on the Jews and the Christians, for example, to whom the message of Prophet Muhammad (peace be upon him) has been conveyed; so they know about it but they do not follow it and follow their own religions instead?

Answer:

They are deemed to be Kafirs (disbelievers) and should be treated as such regarding the rulings in this world and the Hereafter. It will be of no avail for them to adhere to their religions with their Kufr (disbelief) in the Revelation that was sent to our Prophet Muhammad (peace be upon him).

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.

