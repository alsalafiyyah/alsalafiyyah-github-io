---
layout: post
title: "Ruling on reading Bible"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Da'imah no. 8852-2"
category: [nonmuslims, walabara, disbelievers, books]
hijri: Dhu al-Hijjah 8, 1441 AH
date: 2020/07/29
shaykhs: 
 - Shaykh Abdul-Aziz ibn Baz
 - Shaykh Abdul-Razzaq al-Afify
 - Shaykh Abdullah ibn Ghudayyan
 - Shaykh Abdullah ibn Qa'ud
---

Question: 

What is the ruling on reading the Bible?

Answer:

The Divine Books revealed before the Qur'an now include many distortions, alterations, and omissions, as Allah says in the Qur'an. Therefore, it is not permissible for Muslims to read any of these Books unless they have deep-rooted knowledge of the Din (religion) and want to explain the distortions and inconsistencies in these books.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.

