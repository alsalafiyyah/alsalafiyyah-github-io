---
layout: post
title: "Evidence on the invalidity of the Christian creed"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Da'imah no. 16953-5"
category: [nonmuslims, walabara, disbelievers]
hijri: Dhu al-Hijjah 8, 1441 AH
date: 2020/07/29
shaykhs: 
 - Shaykh Abdul-Aziz ibn Baz
 - Shaykh Abdul-Aziz Aal al-Shaykh
 - Shaykh Bakr Abu Zayd
 - Shaykh Salih Fawzan
---

Question: 

What is the evidence on the invalidity of the Christian 'Aqidah (creed)?

Answer:

All religions, whether Christianity or any other, should not be followed after the mission of the Prophet (peace be upon him); everyone should embrace Islam and follow Prophet Muhammad (peace be upon him), according to Allah's saying, "**Say (O Muhammad صلى الله عليه وسلم to mankind): "If you (really) love Allâh then follow me (i.e. accept Islâmic Monotheism, follow the Qur’ân and the Sunnah), Allâh will love you and forgive you your sins.**" [Aal Imran: 31] 

He (Exalted be He) also says, "**Say (O Muhammad صلى الله عليه وسلم): "O mankind! Verily, I am sent to you all as the Messenger of Allâh - to Whom belongs the dominion of the heavens and the earth. Lâ ilâha illa Huwa (none has the right to be worshipped but He). It is He Who gives life and causes death. So believe in Allâh and His Messenger (Muhammad صلى الله عليه وسلم), the Prophet who can neither read nor write (i.e. Muhammad صلى الله عليه وسلم), who believes in Allâh and His Words [(this Qur’ân), the Taurât (Torah) and the Injeel (Gospel) and also Allâh’s Word: "Be!" - and he was, i.e. ‘Isâ (Jesus) son of Maryam (Mary), عليهما السلام], and follow him so that you may be guided.**" [al-A'raf: 158]

In addition, Christianity was distorted and beliefs of Shirk (associating others with Allah in His Divinity or worship) invaded it, such as believing that Jesus Christ is Allah's Son, Allah Himself, or the third Ilah (God). It is a false religion.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.

