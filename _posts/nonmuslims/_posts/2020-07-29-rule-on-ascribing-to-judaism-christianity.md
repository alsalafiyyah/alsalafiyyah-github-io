---
layout: post
title: "Ruling on ascribing a person to Judaism or Christianity"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Da'imah no. 3418-2"
category: [nonmuslims, disbelievers, walabara]
hijri: Dhu al-Hijjah 8, 1441 AH
date: 2020/07/29
shaykhs: 
 - Shaykh Abdul-Aziz ibn Baz
 - Shaykh Abdullah ibn Qa'ud
 - Shaykh Abdullah ibn Ghudayyan
---

Question: 

What makes a person a Jew or Christian, is it by performing their deeds or studying their knowledge?

Answer:

A person becomes a Jew if they believe in the beliefs of the Jews and act upon them. Likewise, they become a Christian if they believe in the beliefs of the Christians and act upon them. Just studying their knowledge, beliefs, and practices to understand the falsehoods and refute them, does not make someone a Jew or a Christian.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.

