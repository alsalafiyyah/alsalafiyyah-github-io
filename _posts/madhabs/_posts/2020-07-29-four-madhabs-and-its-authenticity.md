---
layout: post
title: "The Four Juristic Schools and their authenticity"
publisher: "alsalafiyyah@icloud.com"
source: "Majmu' Fatawa wa Maqalat p. 7/152"
category: [madhabs, malikis, shafiis, hanbalis, hanafis]
hijri: Dhu al-Hijjah 8, 1441 AH
date: 2020/07/29
shaykhs: Shaykh Abdul-Aziz ibn Baz
---

Question: 

Are the Four Madh-habs: Al-Hanbaliyyah, Al-Shafi'iyyah, Al-Malikiyyah, and Al-Hanafiyyah, true? When did they appear?

Answer:

These Four Madh-habs are well-known schools which have spread during the second century and the later centuries. As for the Madh-hab (School of Jurisprudence) of Abu Hanifah, it was known and spread in the second century as well as the Madh-hab of Imam Malik which spread during the second century too. As for the Madh-habs of Imam Al-Shafi'i and Ahmad, they spread during the third century. All of these scholars sought goodness and guidance and their target was to seek the truth which is implied in the Book of Allah and the Sunnah of the Messenger (peace be upon him). This does not mean that each one of them is infallible and errorless but each one has his own mistakes according to the knowledge they gained from the Sunnah and the Book of Allah (Glorified and Exalted be He).

They may miss some knowledge from the Book of Allah and the Sunnah of the Prophet (peace be upon him) and pass a fatwa according to the knowledge which they already have. These are well-established religious matters according to the people of knowledge.

Likewise, the people of knowledge such as Al-Awza'i, Ishaq ibn Rahawayh Sufyan Al-Thawry, Sufyan ibn 'Uyaynah, Waki` ibn Al-Jarrah and other well known scholars; each one of them observed Ijtihad (juristic effort to infer expert legal rulings) according to the knowledge that they received, so whoever was correct, has received double reward and whoever went wrong, would have the reward of his Ijtihad despite the error made. This was reported in the Sunnah from the Prophet (peace be upon him). May Allah grant us success.
