﻿---
layout: post
title: "Ruling on congratulating others in the new Gregorian or Hijri years or the Mawlid"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Daimah no. 19991-1"
category: [mawlids]
hijri: Dhu al-Hijjah 6, 1441 AH
date: 2020/07/27
shaykhs: 
 - Shaykh Abdullah ibn Ghudayyan
 - Shaykh Salih Fawzan
 - Shaykh Abdul-Aziz Aal al-Shaykh
 - Shaykh Bakr Abu Zayd
---

Question: 

Is it permissible to congratulate non-Muslims on the occasion of the new Gregorian year, the new Hijri (lunar) year, and the Mawlid (the Prophet’s birthday)?
 
Answer:

It is not permissible to offer congratulations on such occasions, because it is not Mashru‘ (Islamically lawful) to celebrate them.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.
