﻿---
layout: post
title: "Prophet's songs"
publisher: "alsalafiyyah@icloud.com"
category: [mawlids]
source: "Fatwas of the Permanent Committee of KSA no. 18440-2"
hijri: Dhul-Qa'dah 20, 1441 AH
shaykhs: 
 - Shaykh Ibn Baz
 - Shaykh Abdul-Aziz Aal Al-Shaykh
 - Shaykh Salih Fawzan
 - Shaykh Abdullah ibn Ghudayyan
 - Shaykh Bakr abu Zayd
---

Question: 

Many Muslims celebrate the Prophethood of our Messenger (peace be upon him) with musical songs just as the Jews and Christians do i.e. they mention Allah (Exalted be He) while music is played. What is the ruling on this practice? 

Answer: 

Muslims should express their love for the Messenger of Allah (peace be upon him) by following him, emulating his Sunnah (whatever is reported from the Prophet), and being guided by his guidance. Allah (Exalted be He) says: "Say (O Muhammad صلى الله عليه وسلم to mankind): "If you (really) love Allâh then follow me (i.e. accept Islâmic Monotheism, follow the Qur’ân and the Sunnah), Allâh will love you and forgive you your sins. And Allâh is Oft-Forgiving, Most Merciful." [Aal 'Imran: 31]

On the other hand, musical songs cannot show love to the Prophet (peace be upon him). Rather, they are manifestations of diversion and a hindrance from Dhikr (Remembrance of Allah). Such songs harden the hearts and divert people from Allah and His Straight Path.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions. 

[^1]: Sahih Muslim (Book on Friday, no. 867); Sunan An-Nasa'i (Book on the two `Eid Prayers, no. 1578); Musnad Ahmad p. 3/371; Sunan ad-Darimi (Introduction, no. 206.)
