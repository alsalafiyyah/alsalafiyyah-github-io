---
layout: audio
title: "How to know the Bid'ah and its divisions"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Nur 'Ala al-Darb of Ibn Baz tape no."
category: ["basic", audios]
mp3: https://files.zadapps.info/binbaz.org.sa/fatawa/nour_3la_aldarb/nour_512/51202.mp3
link: https://cutt.ly/4dB5HEc
hijri: Dhul-Hijjah 18, 1441 AH
date: 2020/08/8
shaykhs: Shaykh Abdul-Aziz ibn Baz
---

Question: 

How can we know the Bid'ah and its divisions, respected Shaykh?

Answer: 

Bid'ah refers to whatever people introduce in religion which is in contradiction with the Shariah (Islamic law). This is called "Bid'ah". Anything newly- introduced in religion is called "Bid'ah" as the examples we have mentioned, such as celebrating the Mawlid (birthday) of the Prophet (peace be upon him), building Masjids (mosques) and domes over graves. All these are condemned Bid'ahs. 

They also include the Bid'ah of Jahmiyyah (a deviant Islamic sect denying some Attributes of Allah, claiming they are ascribed to people and cannot be ascribed to Allah) regarding toe Attributes and Names of Allah, that of Mu'tazilah (a deviant Islamic sect claiming that those who commit major sins are in a state between belief and disbelief) regarding the Attributes of Allah and the claim of Mu'tazilah that the perpetrator of a major sin is in an in-between-state of belief and disbelief. 

These are the Bid'ahs made by stray people. 
