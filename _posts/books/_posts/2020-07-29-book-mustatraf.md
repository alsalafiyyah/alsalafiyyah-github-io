---
layout: post
title: "The book entitled Al-Mustatraf"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Da'imah no. 21561-3"
category: [books, sadaqah, charity]
hijri: Dhu al-Hijjah 8, 1441 AH
date: 2020/07/29
shaykhs: 
 - Shaykh Abdullah ibn Ghudayyan
 - Shaykh Abdul-Aziz Aal al-Shaykh
 - Shaykh Bakr Abu Zayd
 - Shaykh Salih Fawzan
---

Question: 

What is your opinion regarding the book entitled "Al-Mustatraf", Is it useful?

Answer:

"Al-Mustatraf" includes good and bad information. Therefore, one should not rely on it either regarding knowledge or religion. Muslims should not waste their time reading unless it will benefit their Din (religion) and worldly affairs.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.

