---
layout: post
title: "Ruling on Writing fiction"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Daimah no. 6252-7"
category: [misc, books, knowledge]
hijri: Dhu al-Hijjah 11, 1441 AH
date: 2020/07/31
shaykhs: 
 - Shaykh Abdul-Aziz ibn Baz
 - Shaykh Abdul-Razzaq al-Afify
---

Question: 

 Is it permissible for a person to write fiction stories, which are all lies, for children to read and learn from?

Answer:

It is Haram (prohibited) for a person to write such false stories. The stories mentioned the Qur'an, the Sunnah and other factual stories suffice and serve as moral lessons.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.
