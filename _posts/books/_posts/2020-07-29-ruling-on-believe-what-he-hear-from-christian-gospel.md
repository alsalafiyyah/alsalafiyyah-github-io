---
layout: post
title: "Ruling on believe in what he hear from what is mentioned in their Gospel"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Da'imah no. 13167-2"
category: [books, walabara]
hijri: Dhu al-Hijjah 8, 1441 AH
date: 2020/07/29
shaykhs: 
 - Shaykh Abdul-Aziz ibn Baz
 - Shaykh Abdullah ibn Ghudayyan
 - Shaykh Abdul-Razzaq al-Afify
---

Question: 

Can we believe in what we hear from the Christians from what is mentioned in their Injil (Gospel)?

Answer:

The Injil (Gospel) of today is not the same that Allah (Glorified and Exalted be He) sent down. Many distortions, lies, additions, omissions, and blasphemous statements are included. Allah states this fact in the Qur'an in His Statement, "**And verily, among them is a party who distort the Book with their tongues (as they read), so that you may think it is from the Book, but it is not from the Book, and they say: "This is from Allâh," but it is not from Allâh; and they speak a lie against Allâh while they know it.**" [Aal Imran: 78]

Furthermore, He, the Exalted, states: "**Then woe to those who write the Book with their own hands and then say, "This is from Allâh," to purchase with it a little price! Woe to them for what their hands have written and woe to them for that they earn thereby.**" [al-Baqarah: 79] 

A Muslim should not read any of the Jewish or Christian books. The Prophet (peace be upon him) admonished 'Umar when he saw him with a paper from the Tawrah (Torah), then the Prophet (peace be upon him) said, "**Are you confused about it (Islam), O Ibn Al-Khattab! By Him in Whose Hands my soul is, I have brought unto you a white, pure faith. Indeed, if Musa were alive, he would have no other recourse but to follow me.**" [^1] According to the narration of 'Abdullah ibn Thabit, then `Umar said: "**I am pleased with Allah as Lord, Islam as a faith, and Muhammad (peace be upon him) as a Prophet.**"

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.

---
[^1]: Ahmad, Musnad, vol. 3, p. 387; Al-Darimi, Sunan, Introduction, no. 435.
