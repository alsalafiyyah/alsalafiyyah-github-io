---
layout: post
title: "Publishing Islamic books as a Sadaqah Jariyah"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Da'imah no. 20062-2"
category: [books, sadaqah, charity]
hijri: Dhu al-Hijjah 8, 1441 AH
date: 2020/07/29
shaykhs: 
 - Shaykh Abdul-Aziz ibn Baz
 - Shaykh Abdul-Aziz Aal al-Shaykh
 - Shaykh Bakr Abu Zayd
---

Question: 

Does the printing of valid Islamic books benefit a Muslim after death, and can it be considered as knowledge by which people may benefit, as mentioned in the Hadith?

Answer:

The printing of useful books that benefit people in their religious and worldly affairs is a good act, for which the Muslim will be rewarded during their life and it will be of ongoing benefit and reward after their death. This is included in the general meaning of the Hadith on the authority of Abu Hurayrah (may Allah be pleased with him) in which the Messenger of Allah (peace be upon him) is authentically reported to have said: "**When someone dies, their works (righteous acts) come to an end, apart from three: Sadaqah Jariyah (ongoing charity), beneficial knowledge, or a pious child who prays for them (the deceased).**" (Related by Imam Muslim in his "Sahih"; Al-Tirmidhy, Al-Nasa'i and Imam Ahmad) [^1]. 

Everyone who participates in conveying useful knowledge will attain this great reward, whether this participation is by writing, teaching, publishing, issuing or participating in printing, each according to their effort and contribution in the work.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.

---
[^1]: Muslim, Sahih, Book on bequests, no. 1631; Al-Tirmidhy, Sunan, Book on judgments, no. 1376; Al-Nasa'i, Sunan, Book on wills, no. 3651; Abu Dawud, Sunan, Book on wills, no. 2880; Ahmad, Musnad, vol. 2, p. 372; and Al-Darimy, Sunan, Introduction, no. 559.

