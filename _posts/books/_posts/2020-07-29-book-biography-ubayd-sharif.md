---
layout: post
title: "The book entitled Biography of Master Shaykh Ubayd Al-Sharif"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Da'imah no. 21547"
category: [books, sadaqah, charity]
hijri: Dhu al-Hijjah 8, 1441 AH
date: 2020/07/29
shaykhs: 
 - Shaykh Abdul-Aziz Aal al-Shaykh
 - Shaykh Abdullah ibn Ghudayyan
 - Shaykh Bakr Abu Zayd
 - Shaykh Salih Fawzan
---

Question: 

What is the ruling on obtaining the book titled, "Al-Ta'rif bil-Shaykh Sayyidy 'Ubayd Al-Sharif (Biography of Master Shaykh 'Ubayd Al-Sharif)", compiled by Al-Hady Bashawat? Is it reliable?

Answer:

Having examined the abovementioned book, we learned that it contains many dangerous ideas including the following:
1. Recommending building a Masjid (mosque) over the grave of the so-called `Ubayd Al-Sharif, frequenting it and seeking blessing from it.
2. Promoting many Bid`ahs (innovations in Islam) and Shirk-related beliefs (Shirk: associating others with Allah in His Divinity or Worship), including seeking blessings from graves and shrines and Tawassul (invoking Allah through an intermediary) through the righteous both the dead and alive.
3. Approving Bid'ahs associated with acts of `Ibadah (worship) including Adhkar (invocations and remembrances said at certain times on a regular basis), invoking blessings on the Prophet (peace be upon him) and visiting places the abovementioned Shaykh used to visit.
4. Quoting unauthentic Hadiths falsely attributed to the Prophet (peace be upon him) in support of his suggestions.

Accordingly, it is impermissible to publish, print, obtain or promote this book as it calls to Shirk, Bid`ahs, superstitions and satanic practices.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.

