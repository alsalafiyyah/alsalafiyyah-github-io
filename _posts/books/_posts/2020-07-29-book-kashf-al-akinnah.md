---
layout: post
title: "The book entitled Kashf Al-Akinnah Amma Qila Innahu Bid'ah Wahwa Sunnah"
publisher: "alsalafiyyah@icloud.com"
source: "Fatawa Al-Lajnah Ad-Da'imah no. 19185"
category: [books, sadaqah, charity]
hijri: Dhu al-Hijjah 8, 1441 AH
date: 2020/07/29
shaykhs: 
 - Shaykh Abdul-Aziz ibn Baz
 - Shaykh Abdul-Aziz Aal al-Shaykh
 - Shaykh Bakr Abu Zayd
 - Shaykh Salih Fawzan
---

Question: 

What is your opinion about a book entitled "Kashf Al-Akinnah 'Amma Qila 'Innahu Bid'ah Wahwa Sunnah (Revealing the Secrets concerning what is Deemed as Bid'ah [innovation in the religion] while it is a Sunnah [acts, sayings or approvals of the Prophet])" by `Abdul-Wahhab Mahiyyah?

Answer:

After reviewing the mentioned book, I realized that its author is not one of the reliable scholars in respect of the rulings of Shari'ah (Islamic law). He regarded some Bid`ahs as acts of the Sunnah. It is necessary to refer to the books of the verifying scholars known for their religiousness, honesty and conversance with Islamic knowledge.

May Allah grant us success. May peace and blessings be upon our Prophet Muhammad, his family, and Companions.

