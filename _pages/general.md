---
layout: page
title: "General"
active: general
author: "Instagram@Alsalafiyyah"
permalink: /general/
---

<article class="post">
<ul class="posts">
  {% for post in site.categories.basic %}
    {% if post.url %}
    <li><a href="{{ post.url }}">{{ post.title }}</a>
    </li>
    {% endif %}
  {% endfor %}
</ul>
</article>
