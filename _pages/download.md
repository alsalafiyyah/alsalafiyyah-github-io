﻿---
title: Downloads
layout: page
active: downloads
permalink: /downloads/
---

<article class="post">
<table>
 <thead>
  <tr>
  <th>Type</th>
  <th>Title</th>
  <th>Author</th>
  <th>Download</th>
  </tr>
 </thead>
 <tbody>

 <tr>
  <td><i class="fas fa-file-pdf"></i></td>
  <td>Imam Muhammad bin Abdul-Wahhab: His da'wah and biography</td>
  <td>Shaykh Ibn Baz</td>
  <td><a class="button small icon solid fa-download" href="/pdf/al-wahhab.pdf">Download</a></td>
 </tr>

 <tr>
  <td><i class="fas fa-file-pdf"></i></td>
  <td>Importance of acquiring knowledge for fighting destructive ideas</td>
  <td>Shaykh Ibn Baz</td>
  <td><a class="button small icon solid fa-download" href="/pdf/binbaz-1.pdf">Download</a></td>
 </tr>

 </tbody>
</table>
</article>