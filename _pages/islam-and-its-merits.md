﻿---
layout: page
title: "Islam and its merits"
author: "Instagram@Alsalafiyyah"
category: [basic]
permalink: /islam-and-its-merits/
---

# Islam and its merits

All praise is due to Allah. May peace and blessings be upon the Last Prophet. 

Allah, the Exalted, states, "**This day, I have perfected your religion for you, completed My Favour upon you, and have chosen for you Islâm as your religion." [al-Maidah: 3] Allah (Glorified be He) also says: Truly, the religion with Allâh is Islâm. Allah (Glorified be He) also says: And whoever seeks a religion other than Islâm, it will never be accepted of him, and in the Hereafter he will be one of the losers.**" [Aal Imran: 85]

Islam is to submit to Allah by Tawhid (belief in the Oneness of Allah/monotheism), yielding to Him by obedience and leaving acts of Shirk (associating others with Allah in His Divinity or worship) and those who commit Shirk. Shirk was the belief of the Arabs before the advent of the Da'wah (calling to Islam) of Muhammad (peace be upon him). It is narrated by Al-Bukhari that Abu Raja' Al-'Utaridy said, "we used to worship a certain stone, but if we found a better stone, we would throw the first stone away and worship the other one. If we did not find any stone to worship, we would grasp a handful of dust, pour goat milk over it, then we would circumambulate it."

Islam is to submit to Allah by Tawhid (belief in the Oneness of Allah/monotheism), yielding to Him by obedience and leaving acts of Shirk (associating others with Allah in His Divinity or worship) and those who commit Shirk. Shirk was the belief of the Arabs before the advent of the Da'wah (calling to Islam) of Muhammad (peace be upon him). It is narrated by Al-Bukhari that Abu Raja' Al-'Utaridy said, we used to worship a certain stone, but if we found a better stone, we would throw the first stone away and worship the other one. If we did not find any stone to worship, we would grasp a handful of dust, pour goat milk over it, then we would circumambulate it.

As for the condition of people before the Mission of the Prophet (peace be upon him), the Noble Qur'an referred to them in many Ayahs (Qur'anic verses). For example, when Allah the Glorified and Exalted, states, "**And they worship besides Allâh things that harm them not, nor profit them, and they say: “These are our intercessors with Allâh.**” [Yunus: 18]

Allah (Glorified be He) also states, "**And those who take Auliyâ’ (protectors, helpers, lords, gods) besides Him (say): “We worship them only that they may bring us near to Allâh.**” [al-Zumar: 3]

Allah (Glorified be He) also says: "**Verily, We made the Shayâtin (devils) Auliyâ’ (protectors and helpers) for those who believe not. And when they commit a Fâhishah (evil deed, going round the Ka‘bah in naked state, and every kind of unlawful sexual intercourse), they say: "We found our fathers doing it, and Allâh has commanded it on us." Say: "Nay, Allâh never commands Fâhishah. Do you say of Allâh what you know not?**" [al-A'raf: 27-28] 

"**...surely they took the Shayâtîn (devils) as Auliyâ’ (protectors and helpers) instead of Allâh, and think that they are guided.**" [al-A'raf: 30] 

Allah (Glorified be He) also says: "**And they assign to Allâh a share of the tilth and cattle which He has created, and they say: "This is for Allâh according to their claim, and this is for our (Allâh’s so-called) partners." But the share of their (Allâh’s so-called) "partners" reaches not Allâh, while the share of Allâh reaches their (Allâh’s so-called) "partners"! Evil is the way they judge!**" [al-An'am: 136]

There are many Ayahs that express this meaning. Moreover, the authentic Hadiths narrated from the Messenger of Allah (peace be upon him) and the writings of the compilers of Sirah (the Prophet's biography), historians and trusted scholars on peoples' conditions mentioned that they had different forms of Shirk before the Mission of the Prophet (peace be upon him). Some would worship idols and sculptures. Others used to worship the dead, the sun, the moon, or the stars, etc. Therefore, the Messenger of Allah (peace be upon him) called them to worship Allah alone and abandon all false deities that they and their ancestors worshipped. Allah, the Glorified and Exalted, states, "**Say (O Muhammad صلى الله عليه وسلم): "O mankind! Verily, I am sent to you all as the Messenger of Allâh - to Whom belongs the dominion of the heavens and the earth. Lâ ilâha illa Huwa (none has the right to be worshipped but He). It is He Who gives life and causes death. So believe in Allâh and His Messenger (Muhammad صلى الله عليه وسلم), the Prophet who can neither read nor write (i.e. Muhammad صلى الله عليه وسلم), who believes in Allâh and His Words [(this Qur’ân), the Taurât (Torah) and the Injeel (Gospel) and also Allâh’s Word: "Be!" - and he was, i.e. ‘Isâ (Jesus) son of Maryam (Mary), عليهما السلام], and follow him so that you may be guided.**" [al-A'raf: 158] 

Allah (Glorified be He) also says: "**(This is) a Book which We have revealed unto you (O Muhammad صلى الله عليه و سلم) in order that you might lead mankind out of darkness (of disbelief and polytheism) into light (of belief in the Oneness of Allâh and Islâmic Monotheism) by their Lord’s Leave to the Path of the All-Mighty, the Owner of all Praise.**" [Ibrahim: 1]

Allah (Glorified be He) also says: "O Prophet (Muhammad صلى الله عليه وسلم)! Verily, We have sent you as witness, and a bearer of glad tidings, and a warner, And as one who invites to Allâh [Islâmic Monotheism, i.e. to worship none but Allâh (Alone)] by His Leave, and as a lamp spreading light (through your instructions from the Qur’ân and the Sunnah - the legal ways of the Prophet" [al-Ahzab:45-46]

Allah (Exalted be He) also says: "**And they were commanded not, but that they should worship Allâh, and worship none but Him Alone (abstaining from ascribing partners to Him)**" [al-Bayyinah:5]

Allah (Glorified be He) also says: :"**O mankind! Worship your Lord (Allâh), Who created you and those who were before you so that you may become Al-Muttaqûn (the pious - See V.2:2).**" [al-Baqarah: 21]

Allah (Glorified be He) also says: "**And your Lord has decreed that you worship none but Him.**" [al-Isra: 23] 

Indeed, there are many Ayahs conveying this very meaning. In many Ayahs, Allah (the Glorified) indicates that these Mushriks (those who associate others with Allah in His Divinity or worship), despite their shirk and Kufr (disbelief), admitted that Allah was their Creator and Sustainer but they worshipped others besides Allah as intermediaries between them and Allah. This was previously mentioned as Allah, the Glorified, states, "**And they worship besides Allâh things that harm them not, nor profit them, and they say: “These are our intercessors with Allâh.**” [Yunus: 18] and other Ayahs that convey the same meaning such as the Ayah in which Allah states, "**Say (O Muhammad صلى الله عليه وسلم): "Who provides for you from the sky and the earth? Or who owns hearing and sight? And who brings out the living from the dead and brings out the dead from the living? And who disposes the affairs?" They will say: "Allâh." Say: "Will you not then be afraid of Allâh’s punishment (for setting up rivals in worship with Allâh)?**" [Yunus: 31] 

Allah (Glorified be He) also says: "**And if you ask them who created them, they will surely say: "Allâh." How then are they turned away (from the worship of Allâh, Who created them)?**"" [al-Zukhruf: 87] There are also many other clearly stated Ayahs corroborating this meaning. Thus, the mission of our Master Prophet Muhammad (peace be upon him) brought about Islam, the last of all faiths, not only for Arabs but for all people. The Prophet (peace be upon him) was sent at a time when all people were in dire need of someone to bring them out of darkness into light.

This great Islamic faith is based on five basic principles, which are its pillars. They are mentioned in the Two Sahih (authentic) Books of Hadith (i.e. Al-Bukhari and Muslim) in the narration of Ibn 'Umar (may Allah be pleased with them both) that the Prophet (peace be upon him) said, "Islam was based on five pillars: testimony that there is no God but Allah and that Muhammad is the Messenger of Allah; performing Salah (prayers), giving Zakah; fasting Ramadan; and performing Hajj (pilgrimage)." [^1]

### The two Testimonies are the first and most important pillar of Islam. 

Such great words are not merely an act of worship to be pronounced by tongue, though by pronouncing them one becomes a Muslim. It is an commitment to act according to what they imply, including the worship of Allah Alone sincerely; the belief that He is the Only One worthy of worship and that worshipping anything or anyone other than Him is Batil (baseless and void).

They imply the necessity of loving Allah, the Glorified, as well as for His Messenger (peace be upon him). Such devotion implies worshipping Allah Alone, glorifying Him, and following the Sunnah (supererogatory act of worship following the example of the Prophet). Allah, the Exalted, states, "Say (O Muhammad صلى الله عليه وسلم to mankind): "**If you (really) love Allâh then follow me (i.e. accept Islâmic Monotheism, follow the Qur’ân and the Sunnah), Allâh will love you and forgive you your sins.**" [al-Imran: 31]

They (i.e. the two testimonies) imply obedience to the Messenger of Allah (peace be upon him) concerning all of his commands. Allah (Glorified be He) says: "**And whatsoever the Messenger (Muhammad صلى الله عليه وسلم) gives you, take it; and whatsoever he forbids you, abstain (from it)**" [al-Hashr: 7]

It is reported by Al-Bukhari and Muslim that the Prophet (peace be upon him) said, “Three (qualities) whoever have them will taste the sweetness of faith: When Allah and His Messenger are more dearer to him than everything else...” [^2]

Thus, the Prophet (peace be upon him) stated, “None of you will believe until I am dearer to him than his father, his children, and all people.” [^3]

### The second pillar: performance of Salah (Prayer). 

It is the second and most important pillar after the two testimonies, as it is the basic foundation of Islam. Indeed, the first act of worship for which Muslim servants will be called to account on the Day of Resurrection is Salah. If Salah is fulfilled, they will achieve success and prosperity, and if it is lacking, they will face loss and frustration. Salah is an act of worship that must be performed at its due time. Allah, the Exalted, states, "**Verily, As-Salât (the prayer) is enjoined on the believers at fixed hours.**" [an-Nisa': 103]

Allah, the Glorified and Exalted, has commanded us to faithfully observe them at their prescribed times, hence He states, "**Guard strictly (five obligatory) As-Salawât (the prayers) especially the middle Salât (i.e. the best prayer - 'Asr ). And stand before Allâh with obedience [and do not speak to others during the Salât (prayers)].**" [al-Baqarah: 238]

Allah, the Glorified and Exalted, has warned those who neglect them and delay them beyond their proper time: "**Then, there has succeeded them a posterity who have given up As-Salât (the prayers) [i.e. made their Salât (prayers) to be lost, either by not offering them or by not offering them perfectly or by not offering them in their proper fixed times] and have followed lusts. So they will be thrown in Hell.**" [maryam: 59] 

Allah (Glorified be He) also says: "**So woe unto those performers of Salât (prayers) (hypocrites), Those who delay their Salât (prayer from their stated fixed times).**" [al-Maun: 4]

Salah is the point of differentiation between Islam and Kufr and Shirk. It is narrated by Muslim in his Sahih that Jabir (may Allah be pleased with him) said: I heard the Messenger of Allah (peace be upon him) saying, "Between a man and disbelief and paganism is the abandonment of Salah (Prayer)." [^4] As, in the Hadith narrated by Buraydah (may Allah be pleased with him), the Prophet stated, "That which differentiates us from the disbelievers and hypocrites is our performance of Salah. He who abandons it becomes a disbeliever." [^5] It is reported by Imam Ahmad and Ahl-ul-Sunan (authors of Hadith compilations classified by jurisprudential themes) through an authentic chain of narrators.

It is obligatory to observe congregational Salah (Prayer) at the mosque, for its great merit. It is narrated that Ibn 'Umar (may Allah be pleased with them both) said that the Messenger of Allah (peace be upon him) stated, "Salah made in congregation is twenty-seven degrees more excellent than the Salah of a single person." (Agreed upon by Al-Bukhari and Muslim) [^6]. It is reported by Al-Bukhari and Muslim that the Messenger of Allah (peace be upon him) considered ordering the houses of those who abandoned congregational Salah to be burned down. The Prophet (peace be upon him) said, "Whoever hears the call (adhan) and does not come to it (the prayer), then there is no prayer for him except with an excuse” [^7]. It is reported by Ibn Majah, Al-Daraqutny, Ibn Hibban and Al-Hakim with an authentic chain of narrators. All these texts indicate the great merit of performing it in congregation.

It is a prerequisite of perfect and accepted Salah by Allah, the Glorified and Exalted, to be performed with humility and tranquility. Allah, the Exalted, states, "**Successful indeed are the believers. Those who offer their Salât (prayers) with all solemnity and full submissiveness.**" [al-Mu'minun: 1-2] The Prophet (peace be upon him) ordered those who were not tranquil while observing Salah to repeat it.

Salah is one of the expressions of equality, brotherhood, union, and united destination towards the Ka'bah as one Qiblah . Salah is the source of comfort and utmost delight for a Mu'min (believer). The Prophet (peace be upon him) stated, "Salah (prayer) has been made my utmost pleasure." [^8] 

The Prophet (peace be upon him) used to turn to Salah whenever he felt any sort of distress. Allah, the Exalted, states, "**Seek help in patience and As-Salât (the prayer).**" [al-Baqarah: 153]

Moreover, he used to say to Bilal, "O Bilal, Comfort us with it (i.e. Salah)." [^9] This is because when a Muslim stands in Salah, he actually stands in front of his Creator, Allah, the Glorified and Exalted. He feels calmness in his heart, tranquility in his soul, submissiveness in all his body, and utmost pleasure with His Lord and God, the Glorified and Exalted.

### The third pillar: Giving Zakah (obligatory charity).

It is a noble social Faridah (obligatory act) that makes a believer realize the supreme principles of Islam including empathy, mercy, love, and cooperation among Muslims. It is not a gift or a favor to give money (as charity), but it is an obligatory duty for in fact one's wealth is merely a trust conferred by Allah on some servants. Allah, the Exalted, states, "**And give them something (yourselves) out of the wealth of Allâh which He has bestowed upon you.**" [an-Nur: 33] 

Allah (Glorified be He) also says: "**Believe in Allâh and His Messenger (Muhammad صلى الله عليه وسلم), and spend of that whereof He has made you trustees. And such of you as believe and spend (in Allâh’s Way), theirs will be a great reward.**" [al-Hadid: 7]

Zakah has been coupled with Salah in many Ayahs and it is because of its great significance that Abu Bakr Al-Siddiq (may Allah be pleased with him) fought the Arab tribes who withheld Zakah due on their wealth. He said, "By Allah! I will fight those who separate between Salah as an act of worship and Zakah (i.e. they offer Salah and refuse to pay Zakah). Thereafter, all the Companions (may Allah be pleased with them) agreed with him doing so. Allah, the Glorified and Exalted, admonished those who fail to give of their wealth in charity, hence He stated, "**And those who hoard up gold and silver [Al-Kanz: the money, the Zakât of which has not been paid] and spend them not in the Way of Allâh, announce unto them a painful torment.**" [at-Tawbah: 34] 

Zakah is obligatory upon every Muslim who possesses any quantity of Nisab (the minimum amount on which Zakah is due) after a whole year passes, except for grain and fruits on which Zakah is due whenever they reach maturity and are harvested regardless of the passing of a year.

Zakah must be paid to its recipients exactly according to the categories mentioned in the Noble Qur'an in Surah At-Tawbah. Allah (Glorified be He) says: "**As-Sadaqât (here it means Zakât) are only for the Fuqarâ’ (poor), and the Masâkin (needy) and those employed to collect (the funds), and to attract the hearts of those who have been inclined (towards Islâm), and to free the captives, and for those in debt, and for Allâh’s Cause, and for the wayfarer (a traveller who is cut off from everything); a duty imposed by Allâh.**" [at-Tawbah: 60]

### The fourth pillar: Observing Sawm (Fast) during Ramadan.

Allah (Glorified be He) says: "**O you who believe! Observing As-Saum (the fasting) is prescribed for you as it was prescribed for those before you, that you may become Al-Muttaqûn (the pious - See V.2: 2).**" [al-Baqaarah: 183] Sawm conditions a Muslim by denying him permitted pleasures and desires for a period of time, and it has health benefits as well as spiritual ones. When observing Sawm, Muslims experience the hunger felt by Muslim brothers who may find neither food nor drink for days, as is the case with some of our Muslim brothers in Africa now.

Ramadan is the best of all months in which Allah revealed the Noble Qur'an. Allah, the Exalted, states, "**The month of Ramadan in which was revealed the Qur’ân, a guidance for mankind and clear proofs for the guidance and the criterion (between right and wrong).**" [al-Baqarah: 185] 

It includes a night which is better than a thousand months. Allah (Glorified be He) says: "**Verily, We have sent it (this Qur’ân) down in the night of Al-Qadr (Decree). And what will make you know what the night of Al-Qadr (Decree) is? The night of Al-Qadr (Decree) is better than a thousand months (i.e. worshipping Allâh in that night is better than worshipping Him a thousand months, i.e. 83 years and 4 months).**" [al-Qadr: 1-3]

Whoever observes Sawm (during this month) out of firm belief and hope for Allah's reward, all his past sins will be forgiven. It is authentically reported that Abu Hurayrah (may Allah be pleased with him) narrated that the Messenger of Allah (peace be upon him) stated, "Whoever fasts the month of Ramadan out of sincere Faith hoping for a reward from Allah, then all his past sins will be forgiven. Whoever observes the night Prayer during Ramadan because of faith and seeking reward from Allah, his past sins will be forgiven; and whoever stands for prayers in the night of Qadr out of sincere Faith and hoping for a reward from Allah, then all his previous sins will be forgiven ." (Agreed upon by Al-Bukhari and Muslim). [^10]

It is obligatory upon a person who observes Sawm to preserve his Sawm by abstaining from Ghibah (backbiting), Namimah (tale-bearing), lying, and idle diversions. He should beware of all other prohibitions. He should recite more of the Noble Qur'an, increase the remembrance of Allah, and exert greater effort in performing acts of worship especially in the last ten days of Ramadan.

### The fifth pillar is Hajj (pilgrimage to Makkah) to the Sacred House (Ka‘bah). 

Allah (Glorified be He) says: "**And Hajj (pilgrimage to Makkah) to the House (Ka‘bah) is a duty that mankind owes to Allâh, for those who are able to undertake the journey.**" [Aal Imran: 97] 

Allah has enjoined Hajj to be performed once in a lifetime, as well as 'Umrah (lesser pilgrimage). They both are obligatory for every free sane adult Muslim who can afford it. Hajj and 'Umrah are valid when performed by a minor but is not an exemption from performing the obligatory Hajj after reaching adulthood and has the ability. A woman who has no Mahram (spouse or permanently unmarriageable relative) to travel with her during Hajj and `Umrah, is exempted according to the Hadiths narrated from the Messenger of Allah (peace be upon him) which prohibits a woman to travel without a Mahram. 

Indeed, Hajj is an Islamic gathering where Muslims meet one another. They come from every deep and distant mountain path and from every country all over the world with their different nationalities, colors, and languages. They are all dressed in the same cloths, standing on the same land, and performing the same acts of worship. There is no difference between the young and the old, the rich and the poor, and the black and the white; they are all equal. Allah, the Glorified, states, "**O mankind! We have created you from a male and a female, and made you into nations and tribes, that you may know one another. Verily, the most honourable of you with Allâh is that (believer) who has At-Taqwâ [i.e. he is one of the Muttaqûn (the pious. See V.2:2)].**" [al-Hujurat: 13]

Hajj Mabrur (the one accepted by Allah) has no other reward but Paradise, as mentioned in the Two Sahih that Abu Hurayrah (may Allah be pleased with him) narrated a Hadith Marfu` (a Hadith narrated from the Prophet with a connected or disconnected chain of narration) saying, "The performance of ‘Umrah is an expiation for the sins committed (between it and the previous one). And the reward of Hajj Mabrur (the one accepted by Allah) is nothing except Paradise." [^11] In the Sahih, it is narrated that the Prophet (peace be upon him) stated, "Whoever performs Hajj for Allah's pleasure and does not have sexual relations with his wife, and does not commit evil or sin then he will return (after Hajj free from all sins) as if he were born anew." [^12]

Islam has other fundamentals that even if are not bases, they exist in the lives of Muslims and are applied to all matters. Among these principles is enjoining good and forbidding evil. Allah (Glorified be He) describes this Ummah (community sharing one belief) as the best Ummah ever raised up for mankind, because they enjoin good and forbid evil. Allah (Glorified be He) says: "**You [true believers in Islâmic Monotheism, and real followers of Prophet Muhammad صلى الله عليه وسلم) and his Sunnah] are the best of peoples ever raised up for mankind; you enjoin Al-Ma‘rûf (i.e. Islâmic Monotheism and all that Islâm has ordained) and forbid Al-Munkar (polytheism, disbelief and all that Islâm has forbidden), and you believe in Allâh.**" [Aal Imran: 110] 

Some Salaf (righteous predecessors) said, "Whoever wishes to be one of the best among these peoples, let him fulfill its condition which is: enjoining good and forbidding evil. There is another important aspect of Islam that should be taken into consideration by Muslims, which is Jihad in the Cause of Allah. It brings about the honor of Muslims, exalts the Word of Allah, and protects Muslim lands against the transgressions of the disbelievers. It has been mentioned in the two Sahih that Ibn 'Umar (may Allah be pleased with them both) narrated that the Messenger of Allah (peace be upon him) stated, "I have been ordered to fight against people until they testify that none has the right to be worshipped but Allah and that Muhammad (peace be upon him) is Allah's Messenger, and offer the prayers perfectly, and give Zakah (obligatory charity), so if they perform all that, then they save their lives and property from me except for Islamic laws, and then their reckoning (accounts) will be done by Allah." [^13]

In Musnad (Hadith compilation of) Imam Ahmad and Jami' Al-Tirmidhy with an authentic chain of narration from Mu`adh (may Allah be pleased with him) that the Prophet (peace be upon him) stated, "The peak of the matter is Islam; the pillar is Salah; and its topmost part is Jihad (Fighting/Struggling in the Cause of Allah)." [^14]

Moreover, Abu Bakr Al-Siddiq (may Allah be pleased with him), in the sermon that he delivered and after which Muslims granted him their Bay`ah (pledge of allegiance), said, "No people will abandon Jihad but Allah will afflict them with humiliation." Jihad enforces Al-Haqq (the Truth), suppresses falsehood, establishes Allah's Law, and protects Muslims and their lands from their enemies' schemes.

Islam is the faith of Fitrah (natural disposition) upon which Allah created mankind. It is the mission of the past Prophets and Messengers. Every Prophet called his people to embrace Islam. Allah (Glorified be He) in His Great Book mentions the Father of all Prophets and the Friend of Allah, Ibrahim (peace be upon him): "**And who turns away from the religion of Ibrâhîm (Abraham) (i.e. Islâmic Monotheism) except him who befools himself? Truly, We chose him in this world and verily, in the Hereafter he will be among the righteous. When his Lord said to him, "Submit (i.e. be a Muslim)!" He said, "I have submitted myself (as a Muslim) to the Lord of the ‘Alamîn (mankind, jinn and all that exists)." And this (submission to Allâh, Islâm) was enjoined by Ibrâhîm (Abraham) upon his sons and by Ya‘qûb (Jacob) (saying), "O my sons! Allâh has chosen for you the (true) religion, then die not except in the Faith of Islâm (as Muslims - Islâmic Monotheism).**" [al-Baqarah: 130-132]

Allah sent His Prophet Muhammad (peace be upon him) with this great faith when People of the Book including the Jews and the Christians were in a state of ignorance and divergence after they distorted and changed the Tawrah (Torah) and Injil (Gospel) yielding to their desires. Therefore, the Jews and the Christians sided with the disbelievers of Quraysh to defeat Muhammad (peace be upon him) and his mission, especially the Jews although they knew about him through their Book and that they were demanded to follow him and believe in his mission. Allah, the Glorified, states, "**Those to whom We gave the Scripture (Jews and Christians) recognise him (Muhammad (peace be upon him) or the Ka‘bah at Makkah) as they recognise their sons.**" [al-Baqarah: 146]

Furthermore, it is reported in Sahih of Muslim from Abu Hurayrah (may Allah be pleased with him) that the Messenger of Allah (peace be upon him) stated, "By Him in Whose hand is the soul of Muhammad, any Jew or Christian amongst this community who hears about me but does not affirm his belief in that with which I have been sent and dies in this state (of disbelief), he shall be but one of the inhabitants of Hellfire." [^15]

Therefore, when our Prophet Muhammad (peace be upon him) settled in Al-Madinah, he sent invitations to the kings of the earth of the time calling them to Islam to bring them out of darkness into the light. It is explained by Rib'y ibn `Amir (may Allah be pleased with him) in a few words, when Rustom, the Persian Commander, asked him, "Who are you?" He answered, "We are people whom Allah sent to bring whomever He wills out of the worship of slaves into the worship of Allah Alone, out of the narrowness of this world into the vastness of this world and the Hereafter, and out of the injustice of other beliefs into the justice of Islam."

This final faith has been revealed to set matters aright and guide people to the right direction, including Tawhid (belief in the Oneness of Allah/monotheism), belief in His Prophets and Messengers, and inviting to what they called of Tawhid and submission to Allah. The religion was revealed while the Jews and the Christians were in great opposition. The Jews were known for their abuse of their prophets; they killed some of them and defamed others unjustly, so what about the infallible and the best of Allah's creatures! The Christians went to extremes in worshipping Jesus claiming that Allah, the Exalted, is one of three. Afterwards, Islam was revealed to establish Al-Haqq and nullify falsehood being just and moderate with neither exaggeration nor negligence.

Allah (Glorified be He) says, "**Thus We have made you [true Muslims - real believers of Islâmic Monotheism, true followers of Prophet Muhammad صلى الله عليه وسلم and his Sunnah (legal ways)], a just (and the best) nation, that you be witnesses over mankind and the Messenger (Muhammad (peace be upn him)) be a witness over you.**" [al-Baqarah: 143] 

Allah (Glorified and Exalted be He) prohibited and warned the People of the Book against exceeding the limits and warned this Community against following their path, stating, "**O people of the Scripture (Christians)! Do not exceed the limits in your religion, nor say of Allâh aught but the truth.**" [an-Nisa: 171] 

It is narrated by Al-Bukhari in his Sahih that 'Umar Ibn Al-Khattab (may Allah be pleased with him) said that the Prophet (peace be upon him) said, "Never deify me the way the Christians deified 'Isa (Jesus), the son of Maryam (Mary), verily I am a servant, so call me the servant of Allah and His Messenger." [^16] 

It is authentically reported from Ibn `Abbas (may Allah be pleased with them both) that the Prophet (peace be upon him) stated, "Do not exceed the limits in your faith for those who preceded you were destroyed due to their excessiveness in faith." [^17]

The merits of Islam are countless; how could they not be while Islam is the way of Allah Who knows all, has absolute wisdom, and irrefutable evidence. He is the All-Wise and the All-Knowing in every matter He ordains and legislates for His servants. Our Messenger (peace be upon him) omitted nothing in calling to the good and guiding Muslims to it and left no evil except that he warned against it. It is narrated in Sahih Muslim that 'Abdullah ibn 'Amr ibn Al-`As (may Allah be pleased with them both) said that the Prophet (peace be upon him) said, "It is the duty of every Prophet whom Allah has sent to guide his followers to what he knew was good for them and warn them against what he knew was bad for them." [^18]

Similarly, it is reported in Musnad (Hadith compilation) of Imam Ahmad with an authentic chain of narrators that Abu Hurayrah (may Allah be pleased with him) said that the Prophet (peace be upon him) said, "I was sent to perfect good character." [^19] The same Hadith was narrated by Al-Hafizh Al-Khara'ity, with a good chain of narration as follows: "I was sent to perfect good morals." [^20]

Finally, we notice nowadays that so many people accept Islam including Kafirs, Mushriks, and People of the Book; the Jews and the Christians. This is indeed an indication of the failure of other faiths and philosophies to bring tranquility, peace, and happiness to people. It is the duty of Muslims, especially the preachers, to expand their activities among these people in order to call them to the way of Allah. However, before doing so we must not forget to adhere to Islam through knowledge and practice. Indeed, mankind is urgently in need of people who will bring them out of darkness into light. 

"**And who is better in speech than he who [says: "My Lord is Allâh (believes in His Oneness)," and then stands firm (acts upon His Order), and] invites (men) to Allâh’s (Islâmic Monotheism), and does righteous deeds, and says: "I am one of the Muslims."**" [Fussilat: 33]

May Allah make us preachers of good, grant us the deep knowledge of our faith and success in calling to it insightfully. Verily, He is the Lord of everything and the Omnipotent. May Allah's Peace and Blessings be upon Muhammad, his family, and Companions!


---

[^1]: Al-Bukhari, Sahih, Book on faith, no. 8; Muslim, Sahih, Book on faith, no. 16; Al-Tirmidhy, Sunan, Book on faith, no. 2609; Al-Nasa*y, Sunan, Book on faith and its laws, no. 5001; and Ahmad ibn Hanbal, Musnad, vol. 2, p. 26.
[^2]: Al-Bukhari, Sahih, Book on faith, no. 16; Muslim, Sahih, Book on faith, no. 43; Al-Tirmidhy, Sunan, Book on faith, no. 2624; Al-Nasa*y, Sunan, Book on faith and its laws, no. 4988; Ibn Majah, Sunan, Book on trials, no. 4033; and Ahmad ibn Hanbal, Musnad, vol. 3, p. 172.
[^3]: Al-Bukhari, Sahih, Book on faith, no. 15; Muslim, Sahih, Book on faith, no. 44; Al-Nasa*y, Sunan, Book on faith and its laws, no. 5013; Ibn Majah, Sunan, Introduction, no. 67; Ahmad ibn Hanbal, Musnad, vol. 3, p. 278; and Al-Darimy, Sunan, Book on heart-softening narrations, no. 2741.
[^4]: Muslim, Sahih, Book on faith, no. 82; Al-Tirmidhy, Sunan, Book on faith, no. 2620; Abu Dawud, Sunan, Book on Al-Sunnah, no. 4678; Ibn Majah, Sunan, Book on performing Prayer and its Sunan, no. 1078; Ahmad ibn Hanbal, Musnad, vol. 3, p. 370; and Al-Darimy, Sunan, Book on Salah, no. 1233.
[^5]: Al-Tirmidhy, Sunan, Book on faith, no. 2621; Al-Nasa*y, Sunan, Book on Salah, no. 463; Ibn Majah, Sunan, Book on performing Prayer and its Sunan, no. 1079; and Ahmad ibn Hanbal, Musnad, vol. 5, p. 346.
[^6]: Al-Bukhari, Sahih, Book on Adhan, no. 645; Muslim, Sahih, Book on Masjids and places for Salah, no. 650; Al-Tirmidhy, Sunan, Book on Salah, no. 215; Al-Nasa*y, Sunan, Book on Imamate, no. 837; Ibn Majah, Sunan, Book on Masjids and congregations, no. 789; Ahmad ibn Hanbal, Musnad, vol. 2, p. 65; and Malik, Al-Muwatta*, Book on call to Prayer, no. 290.
[^7]: Abu Dawud, Sunan, Book on Salah, no. 551; and Ibn Majah, Sunan, Book on Masjids and congregations, no. 793.
[^8]: Al-Nasa'i, Sunan, Book on consorting with one*s women, no. 3940; and Ahmad ibn Hanbal, Musnad, vol. 3, p. 285.
[^9]: Abu Dawud, Sunan, Book on manners, no. 4985; and Ahmad ibn Hanbal, Musnad, vol. 5, p. 371.
[^10]: Al-Bukhari, Sahih, Book on Tarawih Prayer, no. 2014; Muslim, Sahih, Book on travelers* Salah and Salah shortening, no. 760; Al-Tirmidhy, Sunan, Book on fasting, no. 683; Al-Nasa'i, Sunan, Book on fasting, no. 2202; and Ahmad ibn Hanbal, Musnad, vol. 2, p. 241.
[^11]: Al-Bukhari, Sahih, Book on Hajj, no. 1773; Muslim, Sahih, Book on Hajj, no. 1349; Al-Tirmidhy, Sunan, Book on Hajj, no. 933; Al-Nasa'i, Sunan, Book on Hajj rituals, no. 2629; Ibn Majah, Sunan, Book on rituals, no. 2888; Ahmad ibn Hanbal, Musnad, vol. 2, p. 462; and Malik, Al-Muwatta*, Book on Hajj, no. 776.
[^12]: Al-Bukhari, Sahih, Book on Hajj, no. 1521; Muslim, Sahih, Book on Hajj, no. 1350; Al-Tirmidhy, Sunan, Book on Hajj, no. 811; Al-Nasa'i, Sunan, Book on Hajj rituals, no. 2627; Ibn Majah, Sunan, Book on rituals, no. 2889; Ahmad ibn Hanbal, Musnad, vol. 2, p. 248; and Al-Darimy, Sunan, Book on rituals, no. 1796.
[^13]: Al-Bukhari, Sahih, Book on faith, no. 25; and Muslim, Sahih, Book on faith, no. 22.
[^14]: Al-Tirmidhy, Sunan, Book on Faith, no. 2616; and Ibn Majah, Sunan, Book on Trials, no. 3973.
[^15]: Muslim, Sahih, Book on faith, no. 153; and Ahmad ibn Hanbal, Musnad, vol. 2, p. 350.
[^16]: Al-Bukhari, Sahih, Book on prophets, no. 3445; and Ahmad ibn Hanbal, Musnad, vol. 1, p. 24.
[^17]: Ibn Majah, Sunan, Book on rituals, no. 3029; and Ahmad ibn Hanbal, Musnad, vol. 1, p. 215.
[^18]: Muslim, Sahih, Book on rulership, no. 1844; Al-Nasa*y, Sunan, Book on Al-Bay`ah, no. 4191; Ibn Majah, Sunan, Book on trials, no. 3956; and Ahmad Ibn Hanbal, Musnad, vol. 2, p. 191.
[^19]: Ahmad ibn Hanbal, Musnad, vol. 2, p. 381.
[^20]: Ahmad ibn Hanbal, Musnad, vol. 2, p. 381.



